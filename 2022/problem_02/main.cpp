// Problem 2 in https://www.nsa.smm.lt/wp-content/uploads/2022/06/IT_2022_pagr.pdf

#include<iostream>
#include <fstream>
#include <string>
#include <vector>
#include <set>
#include <algorithm>

using namespace std;

struct Excercise {
  string name;
  string time;
  uint duration;
  uint day;
};

void readInputDay(istream &fin, uint day, vector<Excercise>& out) {
  uint countExcercise;
  fin >> countExcercise;

  for (uint i = 0; i < countExcercise; ++i) {
    Excercise entry;
    fin >> entry.name >> entry.time >> entry.duration;
    entry.day = day;
    out.push_back(entry);
  }
}

vector<Excercise> readInput(istream& fin) {
  vector<Excercise> entries;
  uint countDays;
  fin >> countDays;

  for (uint d = 0; d < countDays; ++d) {
    readInputDay(fin, d, entries);
  }
  return entries;
}

vector<string> uniqueSortedExcercises(const vector<Excercise>& items) {
  set<string> vals;
  for (auto it : items) {
    vals.insert(it.name);
  }
  vector<string> names(vals.begin(), vals.end());
  sort(names.begin(), names.end());
  return names;
}

vector<Excercise> filterByName(const vector<Excercise>& entries, const string& name) {
  vector<Excercise> out;
  for (auto it : entries) {
    if (it.name == name) {
      out.push_back(it);
    }
  }
  return out;
}

uint countDays(const vector<Excercise>& items) {
  set<uint> values;
  for (auto it : items) {
    values.insert(it.day);
  }
  return values.size();
}

uint sumDuration(const vector<Excercise>& items) {
  uint sum = 0;
  for (auto it : items) {
    sum += it.duration;
  }
  return sum;
}

uint countOccasions(const vector<Excercise>& items, const string& time) {
  uint counter = 0;
  for (auto it : items) {
    counter += (it.time == time ? 1 : 0);
  }
  return counter;
}

void printDayOfTimeStats(ostream& fout, const vector<Excercise>& items, string time) {
  auto count = countOccasions(items, time);
  if (count) fout << time << " " << count << endl;
}

void printStats(ostream& fout, const vector<Excercise>& entries, const string& name) {
  fout << name << " " << countDays(entries) << " " << sumDuration(entries) << endl;
  printDayOfTimeStats(fout, entries, "Rytas");
  printDayOfTimeStats(fout, entries, "Diena");
  printDayOfTimeStats(fout, entries, "Vakaras");
}

Excercise randomExcercise() {
  return Excercise {
    "asdasd" + random(), "dasdas", 3, 3
  };
}

int main() {
  ifstream fin("input.txt");
  ofstream fout("output.txt");

  auto entries = readInput(fin);
  auto names = uniqueSortedExcercises(entries);
  for (auto name : names) {
    printStats(fout, filterByName(entries, name), name);
  }

  return 0;
}
