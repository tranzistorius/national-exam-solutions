// Problem 1 in https://www.nsa.smm.lt/wp-content/uploads/2022/06/IT_2022_pagr.pdf
#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
using namespace std;

struct Player {
  string name;
  vector<int> times;
  int points;
  int totalTime;
  int totalSolved;
};

bool compare(Player p1, Player p2) {
  if (p1.totalSolved < p2.totalSolved) {
    return false;
  } else if (p1.totalSolved == p2.totalSolved) {
    if (p1.totalTime > p2.totalTime) {
      return false;
    }
    return true;
  }
  return true;
}

int main() {
  // Nuskaityti failiuka
  string line;
  int maximumNumberOfPoints = 0;
  vector<int> timeLimits;
  vector<Player> players;
  vector<int> maxNumberOfPoints;
  ifstream inputFile ("input.txt");
  ofstream outputFile ("output.txt");
  if (!inputFile.is_open()) {
    return -1;
  }
  int numberOfExercises;
  inputFile >> numberOfExercises;
  for (size_t i = 0; i < numberOfExercises; i++) {
    int x;
    inputFile >> x;
    timeLimits.push_back(x);
  }
  for (size_t i = 0; i < numberOfExercises; i++) {
    int x;
    inputFile >> x;
    maxNumberOfPoints.push_back(x);
  }
  // skip a line
  getline(inputFile, line);
  smatch match;
  string regexpString = "([\\w ]+)";
  for (size_t i = 0; i < numberOfExercises; i++) {
    regexpString += " (\\d+)";
  }
  regex expression(regexpString);
  // eat the data
  while (getline(inputFile, line)) {
    regex_match(line, match, expression);
    string name = match[1];
    vector<int> times;
    for (size_t i = 0; i < numberOfExercises; i++) {
      times.push_back(stoi(match[i + 2]));
      // paskaiciuoti taskus
    }
    players.push_back({ name, times, 0, 0 });
  }

  for (size_t j = 0; j < players.size(); j++) {
    int points = 0;
    int totalTime = 0;
    int totalSolved = 0;
    Player& p = players[j];
    for (size_t i = 0; i < p.times.size(); i++) {
      totalTime += p.times[i];
      if (p.times[i] == 0) {
        // nieko
      } else if (timeLimits[i] >= p.times[i]) {
        points += maxNumberOfPoints[i];
        totalSolved += 1;
        // max skaiciu
      } else {
        points += maxNumberOfPoints[i] / 2;
        totalSolved += 1;
        // puse tasku
      }
    }
    if (maximumNumberOfPoints < points) {
      maximumNumberOfPoints = points;
    }
    p.points = points;
    p.totalTime = totalTime;
    p.totalSolved = totalSolved;
  }

  outputFile << maximumNumberOfPoints << endl;

  sort(players.begin(), players.end(), compare);

  for (auto p: players) {
    if (p.points == maximumNumberOfPoints) {
      outputFile << p.name << " " << p.totalSolved << " " <<  p.totalTime << endl;
    }
  }
  
  return 0;
}
